<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content page-big-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <?php $posttype = get_query_var( 'post_type' ); ?>
                        <?php if ($posttype == 'tribe_events') { ?>
                        <h1 itemprop="headline">EVENTOS</h1>
                        <?php $page = 'info-eventos'; ?>
                        <?php $datos = get_page_by_path($page); ?>
                        <p><?php echo $datos->post_content; ?></p>
                        <div class="clearfix"></div>
                        <?php } else { ?>
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <?php the_content(); ?>
                        <?php $posttype = get_query_var( 'eventDisplay' ); ?>
                        <?php if ($posttype == 'single-event') { ?>
                        <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                        <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                        <meta itemprop="url" content="<?php the_permalink() ?>">
                        <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            Compartir
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                <span class="fa-stack fa-xs">
                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x"></i>
                                </span>
                            </a>
                            <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                <span class="fa-stack fa-xs">
                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x"></i>
                                </span>
                            </a>
                        </div>
                        <?php if ( comments_open() ) { comments_template('', true); } ?>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
