<?php

// Register Custom Post Type
function slider() {

    $labels = array(
        'name'                  => _x( 'Slides', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Slide', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'Slider', 'licoteca' ),
        'name_admin_bar'        => __( 'Slider', 'licoteca' ),
        'archives'              => __( 'Archivo de Slides', 'licoteca' ),
        'parent_item_colon'     => __( 'Slide Padre:', 'licoteca' ),
        'all_items'             => __( 'Todos los Slides', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nuevo Slide', 'licoteca' ),
        'add_new'               => __( 'Agregar Nuevo', 'licoteca' ),
        'new_item'              => __( 'Nuevo Slide', 'licoteca' ),
        'edit_item'             => __( 'Editar Slide', 'licoteca' ),
        'update_item'           => __( 'Actualizar Slide', 'licoteca' ),
        'view_item'             => __( 'Ver Slide', 'licoteca' ),
        'search_items'          => __( 'Buscar Slide', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen de Slide', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen de Slide', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen de Slide', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen de Slide', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Slide', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargado a este Slide', 'licoteca' ),
        'items_list'            => __( 'Listado de Slides', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Slides', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Slides', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Slide', 'licoteca' ),
        'description'           => __( 'Slider en homepage', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-slides',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'slider', $args );

}
add_action( 'init', 'slider', 0 );

// Register Custom Post Type
function cocteles() {

    $labels = array(
        'name'                  => _x( 'Cocteles', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Coctel', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'Cocteles', 'licoteca' ),
        'name_admin_bar'        => __( 'Cocteles', 'licoteca' ),
        'archives'              => __( 'Archivo de Cocteles', 'licoteca' ),
        'parent_item_colon'     => __( 'Coctel Padre:', 'licoteca' ),
        'all_items'             => __( 'Todos los Cocteles', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nuevo Coctel', 'licoteca' ),
        'add_new'               => __( 'Agregar Nuevo', 'licoteca' ),
        'new_item'              => __( 'Nuevo Coctel', 'licoteca' ),
        'edit_item'             => __( 'Editar Coctel', 'licoteca' ),
        'update_item'           => __( 'Actualizar Coctel', 'licoteca' ),
        'view_item'             => __( 'Ver Coctel', 'licoteca' ),
        'search_items'          => __( 'Buscar Coctel', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen del Coctel', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen del Coctel', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen del Coctel', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen del Coctel', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Coctel', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargado a este Coctel', 'licoteca' ),
        'items_list'            => __( 'Listado de Cocteles', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Cocteles', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Cocteles', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Coctel', 'licoteca' ),
        'description'           => __( 'Colección de Cócteles', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-lightbulb',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'cocteles', $args );

}
add_action( 'init', 'cocteles', 0 );



// Register Custom Post Type
function thechoice() {

    $labels = array(
        'name'                  => _x( 'The Choice', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Selección', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'The Choice', 'licoteca' ),
        'name_admin_bar'        => __( 'The Choice', 'licoteca' ),
        'archives'              => __( 'Archivo de The Choice', 'licoteca' ),
        'parent_item_colon'     => __( 'Selección Padre:', 'licoteca' ),
        'all_items'             => __( 'Todas las Selecciones', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nueva Selección', 'licoteca' ),
        'add_new'               => __( 'Agregar Nueva', 'licoteca' ),
        'new_item'              => __( 'Nueva Selección', 'licoteca' ),
        'edit_item'             => __( 'Editar Selección', 'licoteca' ),
        'update_item'           => __( 'Actualizar Selección', 'licoteca' ),
        'view_item'             => __( 'Ver Selección', 'licoteca' ),
        'search_items'          => __( 'Buscar Selección', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen Destacada', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Selección', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargada a esta Selección', 'licoteca' ),
        'items_list'            => __( 'Listado de Selecciones', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Selecciones', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Selecciones', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Selección', 'licoteca' ),
        'description'           => __( 'Sección: The Choice', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'taxonomies'            => array( 'custom_thechoice' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-awards',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'thechoice', $args );

}
add_action( 'init', 'thechoice', 0 );

// Register Custom Post Type
function abocados() {

    $labels = array(
        'name'                  => _x( 'A-Bocados', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Entrada', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'A-Bocados', 'licoteca' ),
        'name_admin_bar'        => __( 'A-Bocados', 'licoteca' ),
        'archives'              => __( 'Archivo de A-Bocados', 'licoteca' ),
        'parent_item_colon'     => __( 'Entrada Padre:', 'licoteca' ),
        'all_items'             => __( 'Todas las Entradas', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nueva Entrada', 'licoteca' ),
        'add_new'               => __( 'Agregar Nueva', 'licoteca' ),
        'new_item'              => __( 'Nueva Entrada', 'licoteca' ),
        'edit_item'             => __( 'Editar Entrada', 'licoteca' ),
        'update_item'           => __( 'Actualizar Entrada', 'licoteca' ),
        'view_item'             => __( 'Ver Entrada', 'licoteca' ),
        'search_items'          => __( 'Buscar Entrada', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen Destacada', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Entrada', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargada a esta Entrada', 'licoteca' ),
        'items_list'            => __( 'Listado de Entrada', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Entradas', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Entradas', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Entrada', 'licoteca' ),
        'description'           => __( 'Sección: A-bocados', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'taxonomies'            => array( 'custom_sections' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-carrot',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'abocados', $args );

}
add_action( 'init', 'abocados', 0 );

// Register Custom Post Type
function afteroffice() {

    $labels = array(
        'name'                  => _x( 'After Office', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Entrada', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'After Office', 'licoteca' ),
        'name_admin_bar'        => __( 'After Office', 'licoteca' ),
        'archives'              => __( 'Archivo de After Office', 'licoteca' ),
        'parent_item_colon'     => __( 'Entrada Padre:', 'licoteca' ),
        'all_items'             => __( 'Todas las Entradas', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nueva Entrada', 'licoteca' ),
        'add_new'               => __( 'Agregar Nueva', 'licoteca' ),
        'new_item'              => __( 'Nueva Entrada', 'licoteca' ),
        'edit_item'             => __( 'Editar Entrada', 'licoteca' ),
        'update_item'           => __( 'Actualizar Entrada', 'licoteca' ),
        'view_item'             => __( 'Ver Entrada', 'licoteca' ),
        'search_items'          => __( 'Buscar Entrada', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen Destacada', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Entrada', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargada a esta Entrada', 'licoteca' ),
        'items_list'            => __( 'Listado de Entrada', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Entradas', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Entradas', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Entrada', 'licoteca' ),
        'description'           => __( 'Sección: AfterOffice', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'taxonomies'            => array( 'custom_sections' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-testimonial',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'afteroffice', $args );

}
add_action( 'init', 'afteroffice', 0 );

// Register Custom Post Type
function bibliobar() {

    $labels = array(
        'name'                  => _x( 'BiblioBar', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Entrada', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'BiblioBar', 'licoteca' ),
        'name_admin_bar'        => __( 'BiblioBar', 'licoteca' ),
        'archives'              => __( 'Archivo de BiblioBar', 'licoteca' ),
        'parent_item_colon'     => __( 'Entrada Padre:', 'licoteca' ),
        'all_items'             => __( 'Todas las Entradas', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nueva Entrada', 'licoteca' ),
        'add_new'               => __( 'Agregar Nueva', 'licoteca' ),
        'new_item'              => __( 'Nueva Entrada', 'licoteca' ),
        'edit_item'             => __( 'Editar Entrada', 'licoteca' ),
        'update_item'           => __( 'Actualizar Entrada', 'licoteca' ),
        'view_item'             => __( 'Ver Entrada', 'licoteca' ),
        'search_items'          => __( 'Buscar Entrada', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen Destacada', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Entrada', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargada a esta Entrada', 'licoteca' ),
        'items_list'            => __( 'Listado de Entrada', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Entradas', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Entradas', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Entrada', 'licoteca' ),
        'description'           => __( 'Sección: BiblioBar', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'taxonomies'            => array( 'custom_sections' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-book',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'bibliobar', $args );

}
add_action( 'init', 'bibliobar', 0 );

// Register Custom Post Type
function elbardetoto() {

    $labels = array(
        'name'                  => _x( 'El Bar de Toto', 'Post Type General Name', 'licoteca' ),
        'singular_name'         => _x( 'Entrada', 'Post Type Singular Name', 'licoteca' ),
        'menu_name'             => __( 'El Bar de Toto', 'licoteca' ),
        'name_admin_bar'        => __( 'El Bar de Toto', 'licoteca' ),
        'archives'              => __( 'Archivo de El Bar de Toto', 'licoteca' ),
        'parent_item_colon'     => __( 'Entrada Padre:', 'licoteca' ),
        'all_items'             => __( 'Todas las Entradas', 'licoteca' ),
        'add_new_item'          => __( 'Agregar Nueva Entrada', 'licoteca' ),
        'add_new'               => __( 'Agregar Nueva', 'licoteca' ),
        'new_item'              => __( 'Nueva Entrada', 'licoteca' ),
        'edit_item'             => __( 'Editar Entrada', 'licoteca' ),
        'update_item'           => __( 'Actualizar Entrada', 'licoteca' ),
        'view_item'             => __( 'Ver Entrada', 'licoteca' ),
        'search_items'          => __( 'Buscar Entrada', 'licoteca' ),
        'not_found'             => __( 'No hay resultados', 'licoteca' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'licoteca' ),
        'featured_image'        => __( 'Imagen Destacada', 'licoteca' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'licoteca' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'licoteca' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'licoteca' ),
        'insert_into_item'      => __( 'Insertar en Entrada', 'licoteca' ),
        'uploaded_to_this_item' => __( 'Cargada a esta Entrada', 'licoteca' ),
        'items_list'            => __( 'Listado de Entrada', 'licoteca' ),
        'items_list_navigation' => __( 'Navegación del Listado de Entradas', 'licoteca' ),
        'filter_items_list'     => __( 'Filtro del Listado de Entradas', 'licoteca' ),
    );
    $args = array(
        'label'                 => __( 'Entrada', 'licoteca' ),
        'description'           => __( 'Sección: El Bar de Toto', 'licoteca' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'taxonomies'            => array( 'custom_sections' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-money',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'elbardetoto', $args );

}
add_action( 'init', 'elbardetoto', 0 );

add_action( 'admin_init', 'add_admin_menu_separator' );


function add_admin_menu_separator( $position ) {

    global $menu;

    $menu[ $position ] = array(
        0    =>    '',
        1    =>    'read',
        2    =>    'separator' . $position,
        3    =>    '',
        4    =>    'wp-menu-separator'
    );

}

add_action( 'admin_menu', 'set_admin_menu_separator' );

function set_admin_menu_separator() { do_action( 'admin_init', 29 );  do_action( 'admin_init', 40 );  }



?>
