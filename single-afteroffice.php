<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-afteroffice.png'; ?>
        <div class="taxonomy-bg-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="taxonomy-big-content-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-afteroffice.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>Invitados especiales, Caracas con sus icónicos lugares y el talento de un buen entrevistador son la mezcla perfecta para esta serie de webshows que resalta el elogio al pre-despacho en una personalidad. “Una buena conversación se abre siempre con un sacacorchos”, dijo el humorista estadounidense Evan Esar, nosotros la cerramos -no podía ser de otra manera- con un buen trago y su receta.</p>
                            <div class="embed-responsive embed-responsive-16by9">
                                <div class="single-program-video embed-responsive embed-responsive-16by9">
                                    <?php $link = get_post_meta(get_the_ID(), 'rw_video_url', true); ?>
                                    <?php $arrlink = sum_video_parser($link, true); ?>
                                    <?php echo $arrlink['embed']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="single-taxonomy-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="single-taxonomy-title col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/images/title-afteroffice.png'; ?>" alt="" />
                                <h1><?php the_title(); ?></h1>
                                <span><?php the_date('d|m|Y'); ?></span>
                            </div>
                            <article class="single-taxonomy-content col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                               <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x"></i>
                                    </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'afteroffice', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingr">
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-aside-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <h2><?php the_title(); ?></h2>
                                        <span><?php echo get_the_date('d/m/Y'); ?></span>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
                            </aside>
                            <?php endif; ?>
                        </div>


                    </div>
                     <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
