<div class="single-taxonomy-comments col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <h2>comentarios</h2>
    <ul class="commentlist">
        <?php wp_list_comments( 'type=comment&callback=licoteca_comment' ); ?>
    </ul>
    <?php paginate_comments_links(); ?>
    <?php
    $fields =  array(
        'author' =>
        '<p class="comment-form-author">
        <input id="author" name="author" type="text" placeholder="' . __( 'Nombre', 'licoteca' ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="30" /></p>',

        'email' =>
        '<p class="comment-form-email">
        <input id="email" name="email" type="text" placeholder="' . __( 'Email', 'licoteca' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) .
        '" size="30" /></p>',

        'url' => '',
    );
    ?>
    <?php $args = array(
    'id_form'           => 'commentform',
    'class_form'        => 'comment-form-custom',
    'id_submit'         => 'submit',
    'class_submit'      => 'submit',
    'name_submit'       => 'submit',
    'fields'            =>  apply_filters( 'comment_form_default_fields', $fields ),
    'title_reply'       => __( '<h3 class="comment-form-title">¡ déjanos tu comentario !<h3>', 'licoteca' ),
    'title_reply_to'    => __( ' ', 'licoteca' ),
    'cancel_reply_link' => __( 'Cancel Reply', 'licoteca' ),
    'label_submit'      => __( 'enviar', 'licoteca' ),
    'format'            => 'xhtml',

    'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" class="form-control" rows="1" aria-required="true" placeholder="mensaje">' . '</textarea></p>',
    'must_log_in' => '<p class="must-log-in">' .
    sprintf(
        __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
        wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
    ) . '</p>',

    'logged_in_as' => '<p class="logged-in-as">' .
    sprintf(
        __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'licoteca' ),
        admin_url( 'profile.php' ),
        $user_identity,
        wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
    ) . '</p>',

    'comment_notes_before' => '',
); ?>
    <?php comment_form($args, get_the_ID()); ?>



</div>
