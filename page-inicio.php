<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div id="inicio" class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">

            <div class="slider-img-background">
                <?php $args = array('post_type' => 'slider', 'posts_per_page' => 5, 'order' => 'ASC', 'orderby' => 'date'); ?>
                <?php query_posts($args); ?>
                <?php $i = 1; ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php if ($i == 1) { $active = 'active'; } else { $active = ''; }?>
                <div id="image-slider-<?php echo get_the_ID(); ?>" class="slider-img-background-item slider-img-background-item-<?php echo $active; ?>">
                    <?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" ); ?>
                    <img src="<?php echo $thumbnail_src[0]; ?>" alt="Licoteca - <?php echo get_the_title(); ?>" class="img-responsive" />
                </div>
                <?php $i++; endwhile; wp_reset_query(); ?>
            </div>
            <div class="container">
                <div class="row">

                    <div class="slider-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-comp.png" alt="Licoteca" class="img-responsive hidden-xs" />
                            <div class="clearfix"></div>
                            <div class="slider-text-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php $args = array('post_type' => 'slider', 'posts_per_page' => 5, 'order' => 'ASC', 'orderby' => 'date'); ?>
                                <?php query_posts($args); ?>
                                <?php while (have_posts()) : the_post(); ?>
                                <div id="<?php echo get_the_ID(); ?>" class="slider-text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php the_content(); ?>
                                    <h6><?php echo get_post_meta(get_the_ID(), 'rw_quote_author', true); ?></h6>
                                </div>
                                <?php endwhile; wp_reset_query(); ?>
                            </div>
                            <div class="slider-text-container-extra">
                                <span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/comillas.png" alt="Licoteca - Comillas"></span>
                                <span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/comillas.png" alt="Licoteca - Comillas"></span>
                            </div>
                            <div class="clearfix"></div>
                            <h3>LA CASTELLANA | <strong>CARACAS</strong> | VZLA</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mouse-over">
                        <a data-scroll href="#licoteca">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mouse-over.png" alt="Licoteca - Mouse" />
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section id="licoteca" class="the-about col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="logo-letters col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 wow fadeInDown">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-letters.png" alt="Licoteca - Logo Cortado" class="img-responsive" />
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <div class="about-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php the_content(); ?>
                        <div class="clearfix"></div>
                    </div>
                    <?php if ( is_active_sidebar( 'ads_sidebar' ) ) : ?>
                    <ul id="sidebar" class="ads-container home-ads-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php dynamic_sidebar( 'ads_sidebar' ); ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section id="the-tabs" class="the-tabs col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr  visible-lg visible-md visible-sm hidden-xs">
            <div class="avila-bg"></div>
            <div class="host-image">
                <div class="container">
                    <div class="row">

                        <div class="host-image-mask">
                            <img id="img-jonathan" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/host-01.png" class="hoster-custom-img hoster-custom-img-1" alt="Licoteca - Jonathan">
                            <img id="img-rosanna" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/host-02.png" class="hoster-custom-img hoster-custom-img-2" alt="Licoteca - Rosanna">
                            <img id="img-cesar" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/host-03.png" class="hoster-custom-img hoster-custom-img-3 hoster-custom-img-active" alt="Licoteca - CEsar">
                            <img id="img-morella" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/host-04.png" class="hoster-custom-img hoster-custom-img-4" alt="Licoteca - Morella">
                            <img id="img-toto" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/host-05.png" class="hoster-custom-img hoster-custom-img-5" alt="Licoteca - Jonathan">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="tabs-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li id="jonathan" role="presentation"><a href="#tab-jonathan" aria-controls="jonathan" role="tab" data-toggle="tab">Jonathan Reverón</a></li>
                                <li id="rosanna" role="presentation"><a href="#tab-rosanna" aria-controls="rosanna" role="tab" data-toggle="tab">Rosanna Di Turi</a></li>
                                <li id="cesar" class="active" role="presentation"><a href="#tab-cesar" aria-controls="cesar" role="tab" data-toggle="tab">César Miguel Rondón</a></li>
                                <li id="morella" role="presentation"><a href="#tab-morella" aria-controls="morella" role="tab" data-toggle="tab">Morella Atencio</a></li>
                                <li id="toto" role="toto"><a href="#tab-toto" aria-controls="settings" role="tab" data-toggle="tab">Toto Aguerrevere</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="tab-jonathan">
                                    <div id="tab-inner-jonathan" class="tab-inner-container col-md-10 col-md-offset-1 no-paddingl no-paddingr"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-rosanna">
                                    <div id="tab-inner-rosanna" class="tab-inner-container col-md-10 col-md-offset-1 no-paddingl no-paddingr"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in active" id="tab-cesar">
                                    <div id="tab-inner-cesar" class="tab-inner-container col-md-10 col-md-offset-1 no-paddingl no-paddingr">
                                        <?php
                                        $datos = get_page_by_path('info-thechoice');
                                        echo '<div class="the-tab-logo">';
                                        echo get_the_post_thumbnail($datos);
                                        echo '</div>';
                                        echo '<div class="clearfix"></div>';
                                        echo '<div class="the-tab-info">';
                                        echo $datos->post_content;
                                        $args = array('post_type' => 'thechoice', 'posts_per_page' => 1);
                                        query_posts($args);
                                        while (have_posts()) : the_post();
                                        //                                        echo '<div class="embed-responsive embed-responsive-16by9">';
                                        //                                        echo '<div class="single-program-video embed-responsive embed-responsive-16by9">';
                                        //                                        $link = get_post_meta(get_the_ID(), 'rw_prod_url', true);
                                        //                                        $arrlink = sum_video_parser($link, true);
                                        //                                        echo $arrlink['embed'];
                                        //                                        echo '</div>';
                                        //                                        echo '</div>';
                                        echo '<div class="tab-info-cover col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">';
                                        echo '<a href="' . get_the_permalink() . '">';
                                        the_post_thumbnail('full', array('class' => 'img-responsive'));
                                        echo '</a>';
                                        echo '</div>';
                                        endwhile;
                                        echo '</div>';
                                        ?>
                                        <div class="clearfix"></div>
                                        <div class="tabs-more-content col-md-12 no-paddingl no-paddingr">
                                            <a href="<?php echo home_url('/thechoice'); ?>">
                                                <button>Ver más</button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-morella">
                                    <div id="tab-inner-morella" class="tab-inner-container col-md-10 col-md-offset-1 no-paddingl no-paddingr"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-toto">
                                    <div id="tab-inner-toto" class="tab-inner-container col-md-10 col-md-offset-1 no-paddingl no-paddingr"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
        <section class="the-tabs-mobile col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm visible-xs no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="the-tabs-mobile-container col-lg-12 col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                        <div class="the-tabs-mobile-slider col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tabs-mobile-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/afteroffice-mobile.png);">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-afteroffice.png" alt="Licoteca - AfterOffice" class="img-responsive" />
                                <h3>Jonathan Reverón</h3>
                                <p>Invitados especiales, Caracas con sus icónicos lugares, y el talento de un buen entrevistador son la mezcla perfecta para esta serie de webshows que resalta el elogio al pre despacho en una personalidad. “Una buena conversación se abre siempre con un sacacorchos”, dijo el humorista estadounidense Evan Esar, nosotros la cerramos -no podía ser de otra manera- con un buen trago y su receta.</p>
                                <a href="<?php echo home_url('/afteroffice'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                            </div>
                            <div class="tabs-mobile-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/bibliobar-mobile.png);">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-bibliobar.png" alt="Licoteca - BiblioBar" class="img-responsive" />
                                <h3>Rosanna Di Turi</h3>
                                <p><strong>“Caminante: come, bebe y nada más te importe”.</strong> Esta frase célebre del rey sirio Asurbanipal de Sardanápalo recoge el sentir de esta sección. Semana a semana, los amantes de la enogastronomía se darán banquete con información privilegiada sobre todos los aspectos del buen comer: restaurantes, licores, ingredientes, tendencias, iniciativas y más.</p>
                                <a href="<?php echo home_url('/bibliobar'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                            </div>
                            <div class="tabs-mobile-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/thechoice-mobile.png);">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-thechoice.png" alt="Licoteca - The Choice" class="img-responsive" />
                                <h3>César Miguel Rondón</h3>
                                <p>En Licoteca premiamos la fidelidad. Cada dos semanas encontrarás algunos de nuestros productos, de los más buscados y exitosos, en promoción con precios especiales para ti. Únete a la fiesta. Descubre el The Choice de esta semana haciendo click.</p>
                                <a href="<?php echo home_url('/thechoice'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                            </div>
                            <div class="tabs-mobile-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/abocados-mobile.png);">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-abocados.png" alt="Licoteca - A-Bocados" class="img-responsive" />
                                <h3>Morella Atencio</h3>
                                <p>Recetas, técnicas, ingredientes pero también historias. Amantes del arte culinario comparten parte de sus vivencias y experiencia frente a los fogones, mientras preparan suculentos platos ideados para seducir primero la vista y luego el paladar, en maridaje.</p>
                                <a href="<?php echo home_url('/abocados'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                            </div>
                            <div class="tabs-mobile-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/elbardetoto-mobile.png);">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-elbardetoto.png" alt="Licoteca - el Bar de toto" class="img-responsive" />
                                <h3>Toto Aguerrervere</h3>
                                <p>Una bebida, un lugar, un evento. Temas de actualidad, la movida cultural y social de Caracas, sus sitios más trendy para disfrutar de un buen trago, así como los rincones favoritos de la gente más cool. Todo esto y mucho más aderezado con el ingenio de quien se define a sí mismo como un buhonero intelectual, dotado de un humor inteligente y una particular capacidad de observación.</p>
                                <a href="<?php echo home_url('/elbardetoto'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
        <div class="clearfix"></div>
        <section id="cocteles" class="the-cocktails col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'header_ads_sidebar' ) ) : ?>
                    <ul id="sidebar" class="ads-container home-ads-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php dynamic_sidebar( 'header_ads_sidebar' ); ?>
                    </ul>
                    <?php endif; ?>
                    <div class="cocktails-container col-md-10 col-md-offset-1">
                        <h2>COCTELES</h2>
                        <div class="clearfix"></div>
                        <div class="cocktails-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php $datos = get_page_by_path('info-cocteles'); ?>
                            <?php echo $datos->post_content; ?>
                        </div>
                        <div class="cocktails-slide-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <div class="cocktails-slide-img col-lg-5 col-md-5 col-sm-5 col-xs-12 no-paddingl">
                                <div class="cocktails-slide-img-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php $i = 1; ?>
                                    <?php $args = array('post_type' => 'cocteles', 'posts_per_page' => 6, 'order' => 'DESC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'dest_cocteles', 'field' => 'slug', 'terms' => 'si'))); ?>
                                    <?php $myquery = new WP_Query($args); ?>
                                    <?php while ( $myquery->have_posts() ) { ?>
                                    <?php $myquery->the_post(); ?>
                                    <?php if ($i === 1 ) { $active = 'cocktail-img-active'; } else { $active = 'cocktail-img-inactive'; }?>
                                    <a id="img_coctel_<?php echo $i; ?>" title="<?php echo get_the_title(); ?>" class="cocktails-img <?php echo $active; ?>">
                                        <?php if ( has_post_thumbnail() ) : ?>
                                        <img src="<?php the_post_thumbnail_url('cocteles_img'); ?>" class="img-responsive" alt="Licoteca - <?php echo get_the_title(); ?>" />
                                        <?php endif; ?>
                                    </a>
                                    <?php $i++; } ?>
                                </div>
                                <div class="cocktails-slide-paginator col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <ul>
                                        <?php $i = 1; ?>
                                        <li>
                                            <a onclick="cocktail_changer_ant()" class="cocktail_changer">
                                                <</a>
                                                </li>
                                            <?php while ( $myquery->have_posts() ) { ?>
                                            <?php $myquery->the_post(); ?>
                                            <?php if ($i === 1 ) { $active = 'cocktail-link-active'; } else { $active = 'cocktail-link-inactive'; }?>
                                        <li>
                                            <a id="cocktails-link-<?php echo $i; ?>" onclick="change_coctel(<?php echo $i; ?>)" class="<?php echo $active; ?>">
                                                <?php echo $i; ?>
                                            </a>
                                        </li>
                                        <?php $i++; } ?>
                                        <li><a onclick="cocktail_changer_sig()" class="cocktail_changer">></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="cocktails-slide-info col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-7 no-paddingr col-xs-12">
                                <?php $i = 1; ?>
                                <?php while ( $myquery->have_posts() ) { ?>
                                <?php $myquery->the_post(); ?>
                                <?php if ($i === 1 ) { $active = 'cocktail-info-active'; } else { $active = 'cocktail-info-inactive'; }?>
                                <div id="cocktails-info-<?php echo $i; ?>" class="cocktails-slide-info-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr <?php echo $active; ?>">
                                    <h2><?php the_title(); ?></h2>
                                    <div class="cocktails-slide-info-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <?php $i++; } ?>
                            </div>
                        </div>
                        <div class="cocktails-more-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <a href="<?php echo home_url('/cocteles'); ?>" target="_blank">
                                <button>Ver más</button>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php if ( is_active_sidebar( 'cocteles_ads_sidebar' ) ) : ?>
                    <ul id="sidebar" class="ads-container home-ads-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php dynamic_sidebar( 'cocteles_ads_sidebar' ); ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section id="eventos" class="the-events col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="events-container col-md-10 col-md-offset-1">
                        <h2>NEWS</h2>
                        <?php $datos = get_page_by_path('info-eventos'); ?>
                        <?php echo wpautop($datos->post_content); ?>
                        <!--
<div class="events-container-img col-md-12 no-paddingl no-paddingr">
<img src="<?php /* echo esc_url(get_template_directory_uri()); */ ?>/images/events-test.jpg" alt="" class="img-responsive wow fadeIn" />
</div>
-->
                        <div class="clearfix"></div>
                        <?php $i = 1; $args = array('post_type' => 'post', 'posts_per_page' => 4);  ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <?php if ($i == 1) { ?>
                        <div class="tab-posts-big-container col-md-8 col-sm-8 col-xs-12">
                            <?php } ?>
                            <?php if ($i == 3) { ?>
                            <div class="tab-posts-big-container col-md-4 col-sm-4 col-xs-12">
                                <?php } ?>
                                <div class="tab-posts-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ($i == 1) { $img_size = 'blog_img'; } ?>
                                        <?php if ($i == 2) { $img_size = 'blog_img_small'; } ?>
                                        <?php if ($i == 3) { $img_size = 'tab_side_big'; } ?>
                                        <?php if ($i == 4) { $img_size = 'tab_side_small'; } ?>
                                        <?php the_post_thumbnail($img_size, $defaultatts); ?>
                                        <div class="tab-posts-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                            <h2><?php the_title(); ?></h2>
                                            <span><?php echo get_the_date('d/m/Y'); ?></span>
                                        </div>
                                    </a>
                                </div>
                                <?php if ($i == 2) { ?>
                            </div>
                            <?php } ?>
                            <?php if ($i == 4) { ?>
                        </div>
                        <?php } ?>
                        <?php $i++; endwhile; ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="events-more-content col-md-10 col-md-offset-1">
                        <a href="<?php echo home_url('/noticias'); ?>" target="_blank">
                            <button>Ver más</button>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <section id="contacto" class="the-contact col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="contact-container col-md-10 col-md-offset-1 col-sm-12">
                        <h2>CONTACTO</h2>
                        <div class="contact-shortcode-container col-md-12 no-paddingl no-paddingr">
                            <?php echo do_shortcode('[contact-form-7 id="7" title="Formulario de contacto 1"]'); ?>
                        </div>
                        <div class="clearfix"></div>
                        <h3>Av. Mohedano con Av. Pedro Grases, Urb. la Castellana, Caracas Venezuela</h3>
                        <p>Teléfonos: +58 (212) 2610051 / +58 (212) 2615077 </p>
                        <p>Correo electrónico: <a href="mailto:info@licoteca.com.ve">info@licoteca.com.ve</a> </p>
                        <h5>Para ventas: <a href="mailto:ventas@licoteca.com.ve">ventas@licoteca.com.ve</a> </h5>
                        <h5>Horario de atención: Lunes a sábado de 9:00 am a 9:00 pm. </h5>
                        <h5>Amplio estacionamiento + valet parking. </h5>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <div class="contact-background-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bg-contact.png" alt="Licoteca - Contacto" class="img-responsive wow fadeInUp" />
            <div class="footer-scroll-container col-lg-12 col-md-12 col-sm-12 col-xs-12">

            </div>
        </div>
    </div>
</main>
<?php get_footer('home'); ?>