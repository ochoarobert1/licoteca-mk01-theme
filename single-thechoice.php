<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<div class="overlay-thechoice overlay-hidden"></div>
</div>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-thechoice.png'; ?>
        <div class="taxonomy-bg-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="taxonomy-big-content-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-thechoice.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php the_content(); ?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <div class="single-program-video embed-responsive embed-responsive-16by9">
                                    <?php $link = get_post_meta(get_the_ID(), 'rw_prod_url', true); ?>
                                    <?php $arrlink = sum_video_parser($link, true); ?>
                                    <?php echo $arrlink['embed']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="single-taxonomy-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="single-taxonomy-title col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/images/title-thechoice.png'; ?>" alt="" />
                                <?php $date1 = get_post_meta(get_the_ID(), 'rw_time_1', true); ?>
                                <?php $date2 = get_post_meta(get_the_ID(), 'rw_time_2', true); ?>
                                <h1><?php echo str_replace("-", "|", $date1); ?> / <?php echo str_replace("-", "|", $date2); ?></h1>
                            </div>
                            <article class="single-taxonomy-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php $prod_select = get_post_meta(get_the_ID(), 'rw_prod_select', false); ?>
                                <?php foreach ($prod_select as $prods) { ?>
                                <?php $datos = get_post( $prods ); ?>
                                <div onclick="product_pop(<?php echo $datos->ID; ?>)" class="single-taxonomy-product-item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                    <?php echo get_the_post_thumbnail( $datos->ID, 'product_img', $defaultatts ); ?>
                                    <div class="clearfix"></div>
                                    <?php $product = new WC_Product( $datos->ID ); ?>
                                    <?php echo $product->get_price_html(); ?>
                                    <h2><?php echo $datos->post_title; ?></h2>
                                </div>
                                <?php } ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                            </article>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
