<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-elbardetoto.png'; ?>
        <div class="taxonomy-bg-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="taxonomy-big-content-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-elbardetoto.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php $posttype = get_query_var( 'post_type' ); ?>
                            <?php if (($posttype == 'thechoice') || ($posttype == 'abocados') || ($posttype == 'elbardetoto') || ($posttype == 'afteroffice') || ($posttype == 'bibliobar')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <?php echo $datos->post_content; ?>
                            <?php } ?>
                            <?php wp_reset_postdata(); ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <div class="single-taxonomy-category-chooser col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php $terminos = get_the_terms( get_the_ID(), 'custom_sections' ); ?>
                            <?php foreach($terminos as $terms){  $termino_name = $terms->slug; }?>
                            <?php $terms = get_terms( 'custom_sections' );
                            $count = count( $terms );
                            if ( $count > 0 ) {
                                echo '<ul class="single-taxonomy-category-ul">';
                                foreach ( $terms as $term ) {
                                    if ($termino_name === $term->slug ) {
                                        echo '<li class="active"><a href="'. get_term_link($term) .'">' . $term->name . '</a></li>';
                                    } else {
                                        echo '<li><a href="'. get_term_link($term) .'">' . $term->name . '</a></li>';
                                    }
                                }
                                echo '</ul>';
                            } ?>
                        </div>
                        <div class="single-taxonomy-container col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <article class="single-taxonomy-content col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                                <div class="single-taxonomy-img-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-img-container-mask">
                                        <h1><?php the_title(); ?></h1>
                                        <h3><?php echo get_the_date('d / m / Y'); ?></h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                                <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x"></i>
                                    </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'elbardetoto', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingr">
                                <div class="single-taxonomy-influencer-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/inf-elbardetoto.jpg" alt="" class="img-responsive" />
                                    <div class="single-taxonomy-influencer-mask single-taxonomy-bardetoto-mask">
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/title-elbardetoto.png" alt="" class="img-responsive" />
                                        <h2>TOTO AGUERREVERE</h2>
                                        <p>Columnista de humor político en la Revista Clímax desde 2008. Es autor de los libros Cuentos de Sobremesa y la Hora Loca. Guionista del show Calma Pueblo TV, tiene una sección en la radio a través de la señal Mágica FM en el programa Hoy Por Hoy con Marianella Salazar. Ahora comparte sus escritos sobre los tragos y eventos más trendy de Caracas en El Bar de Toto.</p>
                                    </div>
                                </div>
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                        <div class="single-taxonomy-aside-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                            <h2><?php the_title(); ?></h2>
                                            <span><?php echo get_the_date('d/m/Y'); ?></span>
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile;  ?>
                            </aside>
                            <?php endif; ?>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
