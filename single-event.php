<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="taxonomy-big-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1">
                        <h1 itemprop="headline">EVENTOS</h1>
                        <div class="clearfix"></div>
                        <?php the_content(); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-md-10 col-md-offset-1">
                        <div class="skew-content col-md-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
