<?php get_header('proximamente'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="main-prox-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-comp.png" alt="Licoteca" class="img-responsive animated fadeIn delay-1" />
            <h1 class="animated fadeIn delay-2">Nuevo sitio en construcción</h1>
            <h2 class="animated fadeIn delay-2">Fecha de lanzamiento: <strong>26 | 01 | 2017</strong></h2>
        </div>

    </div>
</main>
<?php get_footer('proximamente'); ?>
