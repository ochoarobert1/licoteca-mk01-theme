/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
    "use strict";
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName('body')[0], x = w.innerWidth || e.clientWidth || g.clientWidth,  y = w.innerHeight || e.clientHeight || g.clientHeight;
    return { width: x, height: y };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = function () {
    "use strict";
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
        if (timers[uniqueId]) { clearTimeout(timers[uniqueId]); }
        timers[uniqueId] = setTimeout(callback, ms);
    };
};

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *    // update the viewport, in case the window size has changed
 *    viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
    // set the viewport using the function above
    "use strict";
    viewport = updateViewportDimensions();
    // if the viewport is tablet or larger, we load in the gravatars
    if (viewport.width >= 768) {
        jQuery('.comment img[data-gravatar]').each(function () {
            jQuery(this).attr('src', jQuery(this).attr('data-gravatar'));
        });
    }
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function ($) {
    "use strict";
    jQuery("#sticker").sticky({topSpacing: 0});
    /** Let's fire off the gravatar function * You can remove this if you don't need it */
    //loadGravatars();
    var owl = $(".slider-text-container");
    owl.owlCarousel({

        // Most important owl features
        singleItem : true,
        transitionStyle : "fade",

        //Basic Speeds
        slideSpeed : 1200,
        paginationSpeed : 1800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        stopOnHover : false,

        addClassActive : true,

        // Navigation
        navigation : false,
        navigationText : ["prev", "next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : true,
        paginationNumbers: false,

        afterAction: function (elem) {
            var idPost = $('.active .slider-text-content').attr('id');
            if ($('.slider-img-background-item').hasClass("slider-img-background-item-active")) {
                $('.slider-img-background-item').removeClass('slider-img-background-item-active');
            }
            $('#image-slider-' + idPost).addClass('slider-img-background-item-active');
        }
    });


    var owl2 = jQuery('.the-tabs-mobile-slider');
    owl2.owlCarousel({

        // Most important owl features
        singleItem : true,
        transitionStyle : "fade",

        //Basic Speeds
        slideSpeed : 1200,
        paginationSpeed : 1800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        stopOnHover : false,

        addClassActive : true,

        // Navigation
        navigation : false,
        navigationText : ["prev", "next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : false,
        paginationNumbers: false
    });

    var owl3 = jQuery('.the-tabs-single-slider');
    owl3.owlCarousel({

        // Most important owl features
        singleItem : true,
        transitionStyle : "fade",

        //Basic Speeds
        slideSpeed : 1200,
        paginationSpeed : 1800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        stopOnHover : false,

        addClassActive : true,

        // Navigation
        navigation : false,
        navigationText : ["prev", "next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : false,
        paginationNumbers: false
    });

}); /* end of as page load scripts */

var $grid = $('.archive-masonry-container').masonry({
    itemSelector: '.archive-item',
    columnWidth: '.archive-item-masonry',
    gutter: 10
});
// layout Masonry after each image loads
$grid.imagesLoaded().progress(function () {
    "use strict";
    $grid.masonry('layout');
});



/* BEGIN IMG */
jQuery('.afteroffice-affix').click(function () {
    "use strict";

    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-jonathan').addClass('hoster-custom-img-active');
    /* TABS */
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#jonathan').addClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-jonathan').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-afteroffice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-jonathan').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-jonathan').html(resp);
        }
    });
});

jQuery('.bibliobar-affix').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#rosanna').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-rosanna').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-bibliobar'
        },
        beforeSend: function () {
            jQuery('#tab-inner-rosanna').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-rosanna').html(resp);
        }
    });
});

jQuery('.elbardetoto-affix').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-elbardetoto'
        },
        beforeSend: function () {
            jQuery('#tab-inner-toto').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-toto').html(resp);
        }
    });
});

jQuery('.the-choice-affix').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#cesar').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-cesar').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-thechoice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-cesar').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-cesar').html(resp);
        }
    });
});

jQuery('.abocados-affix').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#morella').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-morella').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-abocados'
        },
        beforeSend: function () {
            jQuery('#tab-inner-morella').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-morella').html(resp);
        }
    });
});

jQuery('#img-cesar').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#cesar').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-cesar').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-thechoice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-cesar').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-cesar').html(resp);
        }
    });
});



jQuery('#img-rosanna').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#rosanna').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-rosanna').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-bibliobar'
        },
        beforeSend: function () {
            jQuery('#tab-inner-rosanna').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-rosanna').html(resp);
        }
    });
});

jQuery('#img-morella').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#morella').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-morella').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-abocados'
        },
        beforeSend: function () {
            jQuery('#tab-inner-morella').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-morella').html(resp);
        }
    });
});

jQuery('#img-toto').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-elbardetoto'
        },
        beforeSend: function () {
            jQuery('#tab-inner-toto').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-toto').html(resp);
        }
    });
});




/* END IMG */

/* BEGIN IMG */
jQuery('#img-jonathan').click(function () {
    "use strict";

    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-jonathan').addClass('hoster-custom-img-active');
    /* TABS */
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#jonathan').addClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-jonathan').addClass('active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-afteroffice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-jonathan').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-jonathan').html(resp);
        }
    });
});

jQuery('#img-cesar').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#cesar').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-cesar').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-thechoice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-cesar').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-cesar').html(resp);
        }
    });
});

jQuery('#img-rosanna').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#rosanna').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-rosanna').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-bibliobar'
        },
        beforeSend: function () {
            jQuery('#tab-inner-rosanna').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-rosanna').html(resp);
        }
    });
});

jQuery('#img-morella').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#toto').removeClass('active');
    jQuery('#morella').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-toto').removeClass('active');
    jQuery('#tab-morella').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-abocados'
        },
        beforeSend: function () {
            jQuery('#tab-inner-morella').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-morella').html(resp);
        }
    });
});

jQuery('#img-toto').click(function () {
    "use strict";
    /* TABS */
    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').addClass('hoster-custom-img-active');
    jQuery('#jonathan').removeClass('active');
    jQuery('#cesar').removeClass('active');
    jQuery('#rosanna').removeClass('active');
    jQuery('#morella').removeClass('active');
    jQuery('#toto').addClass('active');
    jQuery('#tab-jonathan').removeClass('active');
    jQuery('#tab-cesar').removeClass('active');
    jQuery('#tab-rosanna').removeClass('active');
    jQuery('#tab-morella').removeClass('active');
    jQuery('#tab-toto').addClass('active');

    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-elbardetoto'
        },
        beforeSend: function () {
            jQuery('#tab-inner-toto').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-toto').html(resp);
        }
    });
});

/* END IMG */

jQuery('#cesar').click(function () {
    "use strict";

    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').addClass('hoster-custom-img-active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-thechoice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-cesar').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-cesar').html(resp);
        }
    });
});
jQuery('#rosanna').click(function () {
    "use strict";

    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').addClass('hoster-custom-img-active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-bibliobar'
        },
        beforeSend: function () {
            jQuery('#tab-inner-rosanna').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-rosanna').html(resp);
        }
    });
});
jQuery('#jonathan').click(function () {
    "use strict";

    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-jonathan').addClass('hoster-custom-img-active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-afteroffice'
        },
        beforeSend: function () {
            jQuery('#tab-inner-jonathan').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-jonathan').html(resp);
        }
    });
});
jQuery('#morella').click(function () {
    "use strict";

    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').addClass('hoster-custom-img-active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-abocados'
        },
        beforeSend: function () {
            jQuery('#tab-inner-morella').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-morella').html(resp);
        }
    });
});
jQuery('#toto').click(function () {
    "use strict";

    jQuery('#img-jonathan').removeClass('hoster-custom-img-active');
    jQuery('#img-rosanna').removeClass('hoster-custom-img-active');
    jQuery('#img-morella').removeClass('hoster-custom-img-active');
    jQuery('#img-cesar').removeClass('hoster-custom-img-active');
    jQuery('#img-toto').addClass('hoster-custom-img-active');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-tab-function.php',
        data: {
            datos: 'info-elbardetoto'
        },
        beforeSend: function () {
            jQuery('#tab-inner-toto').html('<div class="loader-tab"></div>');
        },
        success: function (resp) {
            jQuery('#tab-inner-toto').html(resp);
        }
    });
});

function changeSlide(idPost) {
    "use strict";
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + '/includes/home-slider-function.php',
        data: {
            datos: idPost
        },
        success: function (resp) {
            jQuery('.the-slider').css('background-image', 'url(' + resp + ')');
        }
    });
    return false;
}

var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function (box) {
            "use strict";
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();

$('#nav').affix({
    offset: {
        top: -100,
        bottom: ($('.the-footer').outerHeight(true))
    }
});

$(window).scroll(function () {
    "use strict";
    if ($(window).scrollTop() === 0) {
        $('#first-section').addClass('active');
    }
});

function change_coctel(x) {
    "use strict";
    /* IMAGES - BEGIN */
    jQuery('.cocktails-img').each(function () {
        jQuery(this).removeClass('cocktail-img-active');
        jQuery(this).addClass('cocktail-img-inactive');
    });
    jQuery('#img_coctel_' + x).removeClass('cocktail-img-inactive');
    jQuery('#img_coctel_' + x).addClass('cocktail-img-active');
    /* IMAGES - END */
    /* INFO - BEGIN */
    jQuery('.cocktails-slide-info-container').each(function () {
        jQuery(this).removeClass('cocktail-info-active');
        jQuery(this).addClass('cocktail-info-inactive');
    });
    jQuery('#cocktails-info-' + x).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-info-' + x).addClass('cocktail-info-active');
    /* INFO - END */
    jQuery('.cocktails-slide-paginator ul li a').each(function () {
        jQuery(this).removeClass('cocktail-link-active');
        jQuery(this).addClass('cocktail-link-inactive');
    });
    jQuery('#cocktails-link-' + x).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-link-' + x).addClass('cocktail-link-active');
}

jQuery('.menu-bars').click(function () {
    "use strict";
    if (jQuery('.menu-container-big').hasClass("menu-container-big-show")) {
        jQuery('.menu-container-big').removeClass('menu-container-big-show');
    } else {
        jQuery('.menu-container-big').addClass('menu-container-big-show');
    }
});

function cocktail_changer_sig() {
    "use strict";
    var sig = 0, act = jQuery(".cocktail-img-active").attr("id");
    act = parseInt(act.replace('img_coctel_', ''));
    if (act >= 6) {
        sig = 1;
    } else {
        sig = act + 1;
    }
    /* IMAGES - BEGIN */
    jQuery('.cocktails-img').each(function () {
        jQuery(this).removeClass('cocktail-img-active');
        jQuery(this).addClass('cocktail-img-inactive');
    });
    jQuery('#img_coctel_' + sig).removeClass('cocktail-img-inactive');
    jQuery('#img_coctel_' + sig).addClass('cocktail-img-active');
    /* IMAGES - END */
    /* INFO - BEGIN */
    jQuery('.cocktails-slide-info-container').each(function () {
        jQuery(this).removeClass('cocktail-info-active');
        jQuery(this).addClass('cocktail-info-inactive');
    });
    jQuery('#cocktails-info-' + sig).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-info-' + sig).addClass('cocktail-info-active');
    /* INFO - END */
    jQuery('.cocktails-slide-paginator ul li a').each(function () {
        jQuery(this).removeClass('cocktail-link-active');
        jQuery(this).addClass('cocktail-link-inactive');
    });
    jQuery('#cocktails-link-' + sig).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-link-' + sig).addClass('cocktail-link-active');
}

function cocktail_changer_ant() {
    "use strict";
    var ant = 0, act = jQuery(".cocktail-img-active").attr("id");
    act = parseInt(act.replace('img_coctel_', ''));
    if (act <= 1) {
        ant = 6;
    } else {
        ant = act - 1;
    }
    /* IMAGES - BEGIN */
    jQuery('.cocktails-img').each(function () {
        jQuery(this).removeClass('cocktail-img-active');
        jQuery(this).addClass('cocktail-img-inactive');
    });
    jQuery('#img_coctel_' + ant).removeClass('cocktail-img-inactive');
    jQuery('#img_coctel_' + ant).addClass('cocktail-img-active');
    /* IMAGES - END */
    /* INFO - BEGIN */
    jQuery('.cocktails-slide-info-container').each(function () {
        jQuery(this).removeClass('cocktail-info-active');
        jQuery(this).addClass('cocktail-info-inactive');
    });
    jQuery('#cocktails-info-' + ant).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-info-' + ant).addClass('cocktail-info-active');
    /* INFO - END */
    jQuery('.cocktails-slide-paginator ul li a').each(function () {
        jQuery(this).removeClass('cocktail-link-active');
        jQuery(this).addClass('cocktail-link-inactive');
    });
    jQuery('#cocktails-link-' + ant).removeClass('cocktail-info-inactive');
    jQuery('#cocktails-link-' + ant).addClass('cocktail-link-active');
}

function close_modal() {
    "use strict";
    jQuery('.overlay-thechoice').removeClass('overlay-show');
    jQuery('.overlay-thechoice').addClass('overlay-hidden');
}

smoothScroll.init({
    selector: '[data-scroll]', // Selector for links (must be a class, ID, data attribute, or element tag)
    selectorHeader: null, // Selector for fixed headers (must be a valid CSS selector) [optional]
    speed: 1000, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    callback: function (anchor, toggle) {} // Function to run after scrolling
});
