<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-elbardetoto.png'; ?>
        <div class="taxonomy-bg-container col-md-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1">
                        <div class="taxonomy-big-content-logo col-md-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-bardetoto.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-md-12">
                            <p>Recetas, técnicas, ingredientes pero también historias. Amantes del arte culinario comparten parte de sus vivencias y experiencias frente a los fogones, mientras preparan suculentos platos ideados para seducir primero la vista y luego el paladar, en maridaje</p>

                        </div>
                        <div class="single-taxonomy-container col-md-12">
                            <div class="single-taxonomy-title col-md-12 no-paddingl no-paddingr">
                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/images/title-bardetoto.png'; ?>" alt="" />
                                <h1><?php the_title(); ?></h1>
                                <span><?php the_date('d|m|Y'); ?></span>
                            </div>
                            <article class="single-taxonomy-content col-md-8 no-paddingl">
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                                <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x"></i>
                                    </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'bardetoto', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-md-4 no-paddingr">
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-md-12 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-aside-item-info col-md-12 no-paddingl no-paddingr">
                                        <h2><?php the_title(); ?></h2>
                                        <span><?php echo get_the_date('d/m/Y'); ?></span>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
                            </aside>
                            <?php endif; ?>
                        </div>


                    </div>
                     <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-md-10 col-md-offset-1">
                        <div class="skew-content col-md-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
