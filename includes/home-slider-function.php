<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_licoteca/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/licoteca/wp-load.php' );
}
global $wpdb;

$datos = $_POST['datos'];

$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $datos ), "full" );
echo $thumbnail_src[0];
?>
