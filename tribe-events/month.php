<?php
/**
 * Month View Template
 * The wrapper template for month view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

do_action( 'tribe_events_before_template' );

// Tribe Bar
tribe_get_template_part( 'modules/bar' );

// Main Events Content
tribe_get_template_part( 'month/content' );

do_action( 'tribe_events_after_template' );
?>

<h2 class="tribe-events-page-title">Eventos Anteriores</h2>
<div class="tribe-events-loop">

    <?php while ( have_posts() ) : the_post(); ?>
    <?php do_action( 'tribe_events_inside_before_loop' ); ?>

    <!-- Month / Year Headers -->
    <?php tribe_events_list_the_date_headers(); ?>

    <!-- Event  -->
    <?php
    $post_parent = '';
    if ( $post->post_parent ) {
        $post_parent = ' data-parent-post-id="' . absint( $post->post_parent ) . '"';
    }
    ?>
    <div id="post-<?php the_ID() ?>" class="col-md-4 <?php tribe_events_event_classes() ?>" <?php echo $post_parent; ?>>
        <?php tribe_get_template_part( 'list/single', 'event' ) ?>
    </div>


    <?php do_action( 'tribe_events_inside_after_loop' ); ?>
    <?php endwhile; ?>

</div><!-- .tribe-events-loop -->
