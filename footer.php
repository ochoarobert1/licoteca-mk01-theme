<div class="clearfix"></div>
<footer class="container-fluid"  role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer the-footer-inner col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-letters.png" alt="" class="img-responsive" />
                        <div class="mouse-over mouse-over-footer" >
                            <a data-scroll href="#top-page">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mouse-over-footer.png" alt="Licoteca - Mouse" />
                            </a>
                        </div>
                    </div>

                    <div class="footer-copy col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-2 hidden-sm hidden-xs"></div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <h3>Av. Mohedano con Av. Pedro Grases, Urb. la Castellana, <span>Caracas Venezuela</span></h3>
                            <h4>Todos los derechos reservados © www.licoteca.com.ve</h4>
                            <h5>UX/UI Diseño OneTwo CreativeGroup™ / Desarrollo por <a href="http://corporaciond1.com/" target="_blank" title="Corporación d1 - cualquier cosa en cualquier momento">Corporación D1</a></h5>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <a href="https://www.instagram.com/Licoteca/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://www.twitter.com/LicotecaVE/" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.facebook.com/LicotecaVE/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
