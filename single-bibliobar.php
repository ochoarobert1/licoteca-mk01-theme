<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-bibliobar.png'; ?>
        <div class="taxonomy-bg-container col-md-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1">
                        <div class="taxonomy-big-content-logo col-md-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-bibliobar.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-md-12">
                            <p><strong>"Caminante: come, bebe y nada más te importe".</strong> Esta frase célebre del rey sirio Asurbanipal de Sardanápalo recoge el sentir de esta sección. Semana a semana, los amantes de la enogastronomía se darán banquete con información privilegiada sobre todos los aspectos del buen comer: restaurantes, licores, ingredientes, tendencias, iniciativas y más.</p>

                        </div>
                        <div class="single-taxonomy-container col-md-12">

                            <article class="single-taxonomy-content col-md-8 no-paddingl">
                                <div class="single-taxonomy-img-container col-md-12 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-img-container-mask">
                                        <h1><?php the_title(); ?></h1>
                                        <h3><?php echo get_the_date('d / m / Y'); ?></h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                                <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                    <span class="fa-stack fa-xs">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x"></i>
                                    </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'bibliobar', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-md-4 no-paddingr">
                                <div class="single-taxonomy-influencer-container col-md-12 no-paddingl no-paddingr">
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/inf-bibliobar.jpg" alt="" class="img-responsive" />
                                    <div class="single-taxonomy-influencer-mask">
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/title-bibliobar.png" alt="" class="img-responsive" />
                                        <h2>ROSANNA DI TURI</h2>
                                        <p>Periodista y editora, especializada en el tema gastronómico desde 1998.
                                            Autora de los libros ABC Del Vino, Ron
                                            de Venezuela y El Legado de Don Armando, merecedores de reconocimientos en los
                                            Gourmand World Cookbook Awards.
                                            Gerente editorial de la revista
                                            Todo en Domingo de El Nacional
                                            y editora de
                                            gastronomiaenvenezuela.com.ve.</p>
                                    </div>
                                </div>
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-md-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                        <div class="single-taxonomy-aside-item-info col-md-12 no-paddingl no-paddingr">
                                            <h2><?php the_title(); ?></h2>
                                            <span><?php echo get_the_date('d/m/Y'); ?></span>
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile;  ?>
                            </aside>
                            <?php endif; ?>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-md-10 col-md-offset-1">
                        <div class="skew-content col-md-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
