<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_licoteca/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wpdb;

$datos = get_page_by_path($_POST['datos']);

?>
<div class="the-tab-logo">
    <?php echo get_the_post_thumbnail($datos); ?>
</div>
<div class="clearfix"></div>
<div class="the-tab-info">
    <?php echo $datos->post_content; ?>
    <?php $post_type = str_replace('info-', '', $_POST['datos']); ?>
    <?php if (($post_type == 'thechoice') || ($post_type == 'abocados') || ($post_type == 'afteroffice')) { ?>
    <?php $args = array('post_type' => $post_type, 'posts_per_page' => 1);  ?>
    <?php query_posts($args); ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="tab-info-cover col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
        <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
        </a>
    </div>
    <!--
<div class="embed-responsive embed-responsive-16by9">
<div class="single-program-video embed-responsive embed-responsive-16by9">
-->
    <?php /* $link = get_post_meta(get_the_ID(), 'rw_prod_url', true); ?>
            <?php $arrlink = sum_video_parser($link, true); ?>
            <?php echo $arrlink['embed']; */ ?>
    <!--
</div>
</div>
-->
    <?php endwhile; ?>
    <?php } ?>
    <?php if (($post_type == 'elbardetoto') || ($post_type == 'bibliobar')) { ?>

    <?php $i = 1; $args = array('post_type' => $post_type, 'posts_per_page' => 4);  ?>
    <?php query_posts($args); ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php if ($i == 1) { ?>
    <div class="tab-posts-big-container col-md-8 col-sm-8 col-xs-12">
        <?php } ?>
        <?php if ($i == 3) { ?>
        <div class="tab-posts-big-container col-md-4 col-sm-4 col-xs-12">
            <?php } ?>
            <div class="tab-posts-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <a href="<?php the_permalink(); ?>">
                    <?php if ($i == 1) { $img_size = 'blog_img'; } ?>
                    <?php if ($i == 2) { $img_size = 'blog_img_small'; } ?>
                    <?php if ($i == 3) { $img_size = 'tab_side_big'; } ?>
                    <?php if ($i == 4) { $img_size = 'tab_side_small'; } ?>
                    <?php the_post_thumbnail($img_size, $defaultatts); ?>
                    <div class="tab-posts-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <h2><?php the_title(); ?></h2>
                        <span><?php echo get_the_date('d/m/Y'); ?></span>
                    </div>
                </a>
            </div>
            <?php if ($i == 2) { ?>
        </div>
        <?php } ?>
        <?php if ($i == 4) { ?>
    </div>
    <?php } ?>
    <?php $i++; endwhile; ?>
    <?php } ?>
</div>
<div class="clearfix"></div>

<div class="tabs-more-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <a href="<?php echo home_url($post_type); ?>"><button>Ver más</button></a>
</div>

