<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-custom-search" type="submit" id="searchsubmit" ><span class="glyphicon glyphicon-search"></span></button>
            </span>
            <label for="s" class="screen-reader-text"><?php _e('Buscar por Términos:','licoteca'); ?></label>
            <input type="search" id="s" name="s" value="" class="form-control" placeholder="<?php _e('Buscar', 'licoteca'); ?>" autocomplete="off">
        </div><!-- /input-group -->
    </div>
</form>

