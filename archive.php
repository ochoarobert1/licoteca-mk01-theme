<?php get_header(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $posttype = get_query_var( 'post_type' ); ?>
        <?php if (($posttype == 'thechoice') || ($posttype == 'abocados') || ($posttype == 'elbardetoto') || ($posttype == 'afteroffice') || ($posttype == 'bibliobar')) { ?>
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-'. $posttype .'.png'; ?>
        <div class="taxonomy-bg-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <?php } ?>
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="taxonomy-big-intro col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php $posttype = get_query_var( 'post_type' ); ?>
                            <?php if (($posttype == 'thechoice') || ($posttype == 'abocados') || ($posttype == 'elbardetoto') || ($posttype == 'afteroffice') || ($posttype == 'bibliobar')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <div class="taxonomy-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php echo get_the_post_thumbnail($datos); ?>
                            </div>
                            <?php echo $datos->post_content; ?>
                            <?php } else { ?>
                            <h1><?php echo get_the_archive_title(); ?></h1>
                            <?php if (($posttype == 'cocteles') || ($posttype == 'tribe_events')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <?php echo $datos->post_content; ?>
                            <?php } ?>
                            <?php } ?>


                        </div>
                        <div class="clearfix"></div>
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <div class="archive-masonry-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" class="archive-item-masonry archive-item col-lg-4 col-md-4 col-sm-6 col-xs-6 <?php echo join(' ', get_post_class()); ?>" role="article">
                                <picture class="archive-item-img col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php if ( has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('cocteles_img', $defaultatts); ?>
                                    </a>
                                    <?php else : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                                    </a>
                                    <?php endif; ?>
                                </picture>
                                <div class="archive-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                                    <h3><?php echo get_the_date('d | m | Y'); ?></h3>
                                </div>
                                <div class="clearfix"></div>
                            </article>
                            <?php endwhile; ?>
                        </div>
                        <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                        </div>
                        <?php else: ?>
                        <article>
                            <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                            <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                        </article>
                        <?php endif; ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
