<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="taxonomy-bg-container col-md-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1">
                        <h2 itemprop="headline" class="single-title text-center">COCTELES</h2>
                        <div class="single-taxonomy-container col-md-12">

                            <article class="single-taxonomy-content col-md-8 no-paddingl">
                                <div class="single-taxonomy-img-container col-md-12 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-img-container-mask">
                                        <h1><?php the_title(); ?></h1>
                                        <h3><?php echo get_the_date('d / m / Y'); ?></h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                                <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                        <span class="fa-stack fa-xs">
                                            <i class="fa fa-square-o fa-stack-2x"></i>
                                            <i class="fa fa-facebook fa-stack-1x"></i>
                                        </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php echo get_the_title(); ?> - <?php the_permalink(); ?>" title="Compartir en Twitter">
                                        <span class="fa-stack fa-xs">
                                            <i class="fa fa-square-o fa-stack-2x"></i>
                                            <i class="fa fa-twitter fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-lg-4 col-md-4 col-sm-4 col-xs-4 hidden-sm hidden-xs no-paddingr">
                                <div class="the-tabs-mobile-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <div class="the-tabs-single-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <div class="tabs-mobile-item tabs-single-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/afteroffice-mobile.png);">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-afteroffice.png" alt="AfterOffice" class="img-responsive" />
                                            <h3>Jonathan Reverón</h3>
                                            <p>Invitados especiales, Caracas con sus icónicos lugares, y el talento de un buen entrevistador son la mezcla perfecta para esta serie de webshows que resalta el elogio al pre despacho en una personalidad. “Una buena conversación se abre siempre con un sacacorchos”, dijo el humorista estadounidense Evan Esar, nosotros la cerramos -no podía ser de otra manera- con un buen trago y su receta.</p>
                                            <a href="<?php echo home_url('/afteroffice'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                                        </div>
                                        <div class="tabs-mobile-item tabs-single-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/bibliobar-mobile.png);">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-bibliobar.png" alt="BiblioBar" class="img-responsive" />
                                            <h3>Rosanna Di Turi</h3>
                                            <p><strong>“Caminante: come, bebe y nada más te importe”.</strong> Esta frase célebre del rey sirio Asurbanipal de Sardanápalo recoge el sentir de esta sección. Semana a semana, los amantes de la enogastronomía se darán banquete con información privilegiada sobre todos los aspectos del buen comer: restaurantes, licores, ingredientes, tendencias, iniciativas y más.</p>
                                            <a href="<?php echo home_url('/bibliobar'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                                        </div>
                                        <div class="tabs-mobile-item tabs-single-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/thechoice-mobile.png);">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-thechoice.png" alt="The Choice" class="img-responsive" />
                                            <h3>César Miguel Rondón</h3>
                                            <p>En Licoteca premiamos la fidelidad. Cada dos semanas seis productos, de los más buscados y exitosos, entrarán en promoción gracias a los descuentos otorgados por nuestros aliados comerciales. Las degustaciones e impulsos de marcas premium también estarán a la orden del día. Únete a la fiesta. Síguenos por las redes sociales para que aproveches todo lo que tenemos para ti.</p>
                                            <a href="<?php echo home_url('/thechoice'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                                        </div>
                                        <div class="tabs-mobile-item tabs-single-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/abocados-mobile.png);">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-abocados.png" alt="A-Bocados" class="img-responsive" />
                                            <h3>Morella Atencio</h3>
                                            <p>Recetas, técnicas, ingredientes pero también historias. Amantes del arte culinario comparten parte de sus vivencias y experiencia frente a los fogones, mientras preparan suculentos platos ideados para seducir primero la vista y luego el paladar, en maridaje.</p>
                                            <a href="<?php echo home_url('/abocados'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                                        </div>
                                        <div class="tabs-mobile-item tabs-single-item" style="background: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/elbardetoto-mobile.png);">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-elbardetoto.png" alt="el Bar de toto" class="img-responsive" />
                                            <h3>Toto Aguerrervere</h3>
                                            <p>Una bebida, un lugar, un evento. Temas de actualidad, la movida cultural y social de Caracas, sus sitios más trendy para disfrutar de un buen trago, así como los rincones favoritos de la gente más cool. Todo esto y mucho más aderezado con el ingenio de quien se define a sí mismo como un buhonero intelectual, dotado de un humor inteligente y una particular capacidad de observación.</p>
                                            <a href="<?php echo home_url('/elbardetoto'); ?>" title="Ver Más"><button class="btn btn-md btn-tabs">ver más</button></a>
                                        </div>
                                    </div>
                                </div>

                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-md-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                        <div class="single-taxonomy-aside-item-info col-md-12 no-paddingl no-paddingr">
                                            <h2><?php the_title(); ?></h2>
                                            <span><?php echo get_the_date('d/m/Y'); ?></span>
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile;  ?>
                            </aside>
                            <?php endif; ?>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-md-10 col-md-offset-1">
                        <div class="skew-content col-md-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
