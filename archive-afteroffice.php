<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $url = esc_url(get_template_directory_uri()) . '/images/bg-afteroffice.png'; ?>
        <div class="taxonomy-bg-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>);"></div>
        <section class="taxonomy-big-container col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="container">
                <div class="row">

                    <?php if (isset($_POST['archivo'])) { ?>
                    <?php /* INICIO ARCHIVE */?>
                    <div class="taxonomy-big-content  col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="taxonomy-big-intro col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php $posttype = get_query_var( 'post_type' ); ?>
                            <?php if (($posttype == 'thechoice') || ($posttype == 'abocados') || ($posttype == 'elbardetoto') || ($posttype == 'afteroffice') || ($posttype == 'bibliobar')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <div class="taxonomy-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php echo get_the_post_thumbnail($datos); ?>
                            </div>
                            <?php echo $datos->post_content; ?>
                            <?php } else { ?>
                            <h1><?php echo get_the_archive_title(); ?></h1>
                            <?php if (($posttype == 'cocteles') || ($posttype == 'tribe_events')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <?php echo $datos->post_content; ?>
                            <?php } ?>
                            <?php } ?>


                        </div>
                        <div class="clearfix"></div>
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <div class="archive-masonry-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" class="archive-item-masonry archive-item col-lg-4 col-md-4 col-sm-6 col-xs-6 <?php echo join(' ', get_post_class()); ?>" role="article">
                                <picture class="archive-item-img col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php if ( has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('cocteles_img', $defaultatts); ?>
                                    </a>
                                    <?php else : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                                    </a>
                                    <?php endif; ?>
                                </picture>
                                <div class="archive-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                                    <h3><?php echo get_the_date('d | m | Y'); ?></h3>
                                </div>
                                <div class="clearfix"></div>
                            </article>
                            <?php endwhile; ?>
                        </div>
                        <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                        </div>
                        <?php else: ?>
                        <article>
                            <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                            <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                        </article>
                        <?php endif; ?>
                    </div>
                    <?php /* FIN ARCHIVE */?>
                    <?php } else { ?>
                    <?php /* INICIO SINGLES */?>
                    <div class="taxonomy-big-content col-md-10 col-md-offset-1">
                        <div class="taxonomy-big-content-logo col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-afteroffice.png" alt="A-bocados" class="img-responsive"/>
                        </div>
                        <div class="taxonomy-content-info col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <?php $posttype = get_query_var( 'post_type' ); ?>
                            <?php if (($posttype == 'thechoice') || ($posttype == 'abocados') || ($posttype == 'elbardetoto') || ($posttype == 'afteroffice') || ($posttype == 'bibliobar')) { ?>
                            <?php $page = 'info-' . $posttype; ?>
                            <?php $datos = get_page_by_path($page); ?>
                            <?php echo $datos->post_content; ?>
                            <?php } ?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <div class="single-program-video embed-responsive embed-responsive-16by9">
                                    <?php $link = get_post_meta(get_the_ID(), 'rw_video_url', true); ?>
                                    <?php $arrlink = sum_video_parser($link, true); ?>
                                    <?php echo $arrlink['embed']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="single-taxonomy-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="single-taxonomy-title col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/images/title-afteroffice.png'; ?>" alt="" />
                                <h1><?php the_title(); ?></h1>
                                <span><?php the_date('d|m|Y'); ?></span>
                            </div>
                            <article class="single-taxonomy-content col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                                <?php the_content(); ?>
                                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                                <meta itemprop="url" content="<?php the_permalink() ?>">
                                <div class="single-taxonomy-sharer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    Compartir
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartir en Facebook">
                                        <span class="fa-stack fa-xs">
                                            <i class="fa fa-square-o fa-stack-2x"></i>
                                            <i class="fa fa-facebook fa-stack-1x"></i>
                                        </span>
                                    </a>
                                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Compartir en Twitter">
                                        <span class="fa-stack fa-xs">
                                            <i class="fa fa-square-o fa-stack-2x"></i>
                                            <i class="fa fa-twitter fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </div>
                                <?php if ( comments_open() ) { comments_template('', true); } ?>
                            </article>
                            <?php $args = array('post_type' => 'afteroffice', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <aside class="single-taxonomy-aside col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingr">
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="single-taxonomy-aside-item col-lg-12 col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                    <div class="single-taxonomy-aside-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <h2><?php the_title(); ?></h2>
                                        <span><?php echo get_the_date('d/m/Y'); ?></span>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
                                <div class="tabs-more-content col-md-12 no-paddingl no-paddingr">
                                    <form action="<?php echo home_url('/afteroffice'); ?>" method="post">
                                        <input type="hidden" name="archivo" value="1" />
                                        <button type="submit">Ver más</button>
                                    </form>
                                </div>
                            </aside>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <div class="taxonomy-skew-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="skew-content col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
