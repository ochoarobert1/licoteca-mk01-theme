<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_functions_overrides.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function licoteca_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltheme-css', get_template_directory_uri() . '/css/owl.theme.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltrans-css', get_template_directory_uri() . '/css/owl.transitions.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME -*/
            wp_register_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL -*/
            wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL -*/
            wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL -*/
            wp_register_style('owltrans-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');
        }

        /*- GOOGLE FONTS -*/
        /*wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Arimo:400,400i,700,700i|Open+Sans+Condensed:300,300i,700|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');*/

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/licoteca-style.min.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/licoteca-mediaqueries.min.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');

        /*- WORDPRESS STYLE -*/
        wp_register_style('wp-initial-style', get_template_directory_uri() . '/style.css', false, $version_remove, 'all');
        wp_enqueue_style('wp-initial-style');
    }
}

add_action('init', 'licoteca_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', false);
    }
    wp_enqueue_script('jquery');
}


function licoteca_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED ON LOCAL  -*/
            wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            wp_enqueue_script('imagesloaded');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '2.0.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

            /*- WOW ON LOCAL -*/
            wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.1.2', true);
            wp_enqueue_script('wow-js');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            wp_enqueue_script('imagesloaded');

            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.pkgd.min.js', array('jquery'), '1.2.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            wp_register_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.0/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            wp_enqueue_script('masonry');

            /*- OWL -*/
            wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

            /*- WOW -*/
            wp_register_script('wow-js', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', array('jquery'), '1.1.2', true);
            wp_enqueue_script('wow-js');

        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'licoteca_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'licoteca', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
    'default-image' => '',    // background image default
    'default-color' => '',    // background color default (dont add the #)
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
)
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function licoteca_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'licoteca_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'licoteca' ),
    'footer_menu' => __( 'Menu Footer', 'licoteca' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'licoteca_widgets_init' );
function licoteca_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'licoteca' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'licoteca' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Widgets seran vistos en Tienda y Categorias de Producto', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    
     register_sidebar( array(
        'name' => __( 'Header Ads Sidebar', 'licoteca' ),
        'id' => 'header_ads_sidebar',
        'description' => __( 'Espacio para publicidad en el header del sitio', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(
        'name' => __( 'Cocteles Ads Sidebar', 'licoteca' ),
        'id' => 'cocteles_ads_sidebar',
        'description' => __( 'Espacio para publicidad en la sección Cocteles', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(
        'name' => __( 'Footer Ads Sidebar', 'licoteca' ),
        'id' => 'footer_ads_sidebar',
        'description' => __( 'Espacio para publicidad en el pie de la pagina', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    
     register_sidebar( array(
        'name' => __( 'Main Ads Sidebar', 'licoteca' ),
        'id' => 'ads_sidebar',
        'description' => __( 'Espacio para publicidad en el home', 'licoteca' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #f5f5f5 !important;
            background-image:url(' . get_template_directory_uri() . '/images/bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important; }
        .login form{
            -webkit-border-radius: 5px;
            border-radius: 5px;
            background-color: rgba(203, 203, 203, 0.5);
            box-shadow: 0px 0px 6px #878787;
        }
        .login label{
            color: black;
            font-weight: 500;
        }
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'licoteca' );
        echo '<a href="http://wordpress.org/" >WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'licoteca' );
        echo '<a href="http://robertochoa.com.ve/" >Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    PRODUCT LIST
-------------------------------------------------------------- */

function prodList() {
    global $wpdb;
    $the_query = "SELECT ID, post_title FROM " . $wpdb->prefix . "posts WHERE post_type LIKE 'product' AND post_status LIKE 'publish' ORDER BY post_date ASC";
    $posts_year_range = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;

    foreach ($posts_year_range as $item){
        $itemkeys[] = $item['ID'];
        $itemvalues[] = $item['post_title'];
    }
    $item_container[] = array_combine($itemkeys, $itemvalues);

    return $item_container[0];
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'licoteca_metabox' );

function licoteca_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'title'    => __( 'Información Extra', 'licoteca' ),
        'pages'    => array( 'abocados', 'afteroffice' ),
        'fields' => array(
        array(
        'id'   => $prefix . 'video_url',
        'name' => __( 'Ruta del Video/Episodio', 'licoteca' ),
        'desc' => __( 'Coloque Aquí la ruta del video <br />(Preferiblemente Youtube / Vimeo)', 'licoteca' ),
        'type' => 'text',
    ),
    )
    );

    $meta_boxes[] = array(
        'title'    => __( 'Información Extra', 'licoteca' ),
        'pages'    => array( 'thechoice' ),
        'fields' => array(
        array(
        'type' => 'heading',
        'name' => esc_html__( 'Período de Tiempo', 'licoteca' ),
        'desc' => esc_html__( 'Semana de Esta Selección', 'licoteca' ),
    ),
        array(
        'name' =>  __( 'Desde:', 'licoteca' ),
        'id'   => $prefix . 'time_1',
        'desc' => __( 'Coloque la Fecha Inicial', 'licoteca' ),
        'type' => 'date',
        'js_options' => array(
        'dateFormat'      => esc_html__( 'dd-mm-yy', 'licoteca' ),
        'changeMonth'     => true,
        'changeYear'      => true,
        'showButtonPanel' => true,
    ),
    ),
        array(
        'name' =>  __( 'Hasta:', 'licoteca' ),
        'id'   => $prefix . 'time_2',
        'desc' => __( 'Coloque la Fecha Final', 'licoteca' ),
        'type' => 'date',
        'js_options' => array(
        'dateFormat'      => esc_html__( 'dd-mm-yy', 'licoteca' ),
        'changeMonth'     => true,
        'changeYear'      => true,
        'showButtonPanel' => true,
    ),
    ),
        array(
        'name' =>  __( 'Video de Selección:', 'licoteca' ),
        'id'   => $prefix . 'prod_url',
        'desc' => __( 'Video de selección, insertar ruta URL', 'licoteca' ),
        'type' => 'text',
    ),
        array(
        'id'   => $prefix . 'prod_select',
        'name' => __( 'Selección de Productos', 'licoteca' ),
        'desc' => __( 'Seleccione los productos de esta semana', 'licoteca' ),
        'type' => 'select_advanced',
        'multiple' => true,
        'options' => prodList(),
    ),
    )
    );

    $meta_boxes[] = array(
        'title'    => __( 'Información Extra - Slide', 'licoteca' ),
        'pages'    => array( 'slider' ),
        'fields' => array(
        array(
        'name' =>  __( 'Autor de la cita', 'licoteca' ),
        'id'   => $prefix . 'quote_author',
        'desc' => __( 'Autor intelectual de la cita a publicar', 'licoteca' ),
        'type' => 'text',
    ),
    )
    );

    $meta_boxes[] = array(
        'title'    => __( 'Información Extra - Producto', 'licoteca' ),
        'pages'    => array( 'product' ),
        'fields' => array(
        array(
        'name' =>  __( 'Origen / Pais', 'licoteca' ),
        'id'   => $prefix . 'product_origen',
        'desc' => __( 'Origen del Producto', 'licoteca' ),
        'type' => 'text',
    ),
        array(
        'name' =>  __( 'Grado de Alcohol', 'licoteca' ),
        'id'   => $prefix . 'product_grade',
        'desc' => __( 'Grado de Alcohol del Producto', 'licoteca' ),
        'type' => 'text',
    ),
        array(
        'name' =>  __( 'Temperatura de Servido', 'licoteca' ),
        'id'   => $prefix . 'product_temp',
        'desc' => __( 'Temperatura de Servido del Producto', 'licoteca' ),
        'type' => 'text',
    ),
        array(
        'name' =>  __( 'Fotografía en Silueta', 'licoteca' ),
        'id'   => $prefix . 'product_silueta',
        'desc' => __( 'Fotografía en Silueta del Producto', 'licoteca' ),
        'type' => 'image_advanced',
    ),

    )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_types.php');

/* --------------------------------------------------------------
    ADD CUSTOM TAXONOMY
-------------------------------------------------------------- */

// Register Custom Taxonomy
function dest_cocteles() {

    $labels = array(
        'name'                       => _x( 'Destacados', 'Taxonomy General Name', 'licoteca' ),
        'singular_name'              => _x( 'Destacado', 'Taxonomy Singular Name', 'licoteca' ),
        'menu_name'                  => __( 'Destacar Cocteles', 'licoteca' ),
        'all_items'                  => __( 'Todos los Destacados', 'licoteca' ),
        'parent_item'                => __( 'Destacado Padre', 'licoteca' ),
        'parent_item_colon'          => __( 'Destacado Padre:', 'licoteca' ),
        'new_item_name'              => __( 'Nuevo Destacado', 'licoteca' ),
        'add_new_item'               => __( 'Añadir Nuevo Destacado', 'licoteca' ),
        'edit_item'                  => __( 'Editar Destacado', 'licoteca' ),
        'update_item'                => __( 'Actualizar Destacado', 'licoteca' ),
        'view_item'                  => __( 'Ver Destacado', 'licoteca' ),
        'separate_items_with_commas' => __( 'Separar Destacados por coma', 'licoteca' ),
        'add_or_remove_items'        => __( 'Agregar o remover Destacados', 'licoteca' ),
        'choose_from_most_used'      => __( 'Escoger de los más Usados', 'licoteca' ),
        'popular_items'              => __( 'Destacados Populares', 'licoteca' ),
        'search_items'               => __( 'Buscar Destacados', 'licoteca' ),
        'not_found'                  => __( 'No hay resultados', 'licoteca' ),
        'no_terms'                   => __( 'No hay Destacados', 'licoteca' ),
        'items_list'                 => __( 'Listado de Destacados', 'licoteca' ),
        'items_list_navigation'      => __( 'Navegación del Listado de Destacados', 'licoteca' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'dest_cocteles', array( 'cocteles' ), $args );

}
add_action( 'init', 'dest_cocteles', 0 );

// Register Custom Taxonomy
function custom_sections() {

    $labels = array(
        'name'                       => _x( 'Secciones', 'Taxonomy General Name', 'licoteca' ),
        'singular_name'              => _x( 'Sección', 'Taxonomy Singular Name', 'licoteca' ),
        'menu_name'                  => __( 'Secciones', 'licoteca' ),
        'all_items'                  => __( 'Todos las Secciones', 'licoteca' ),
        'parent_item'                => __( 'Sección Padre', 'licoteca' ),
        'parent_item_colon'          => __( 'Sección Padre:', 'licoteca' ),
        'new_item_name'              => __( 'Nueva Sección', 'licoteca' ),
        'add_new_item'               => __( 'Añadir Nueva Sección', 'licoteca' ),
        'edit_item'                  => __( 'Editar Sección', 'licoteca' ),
        'update_item'                => __( 'Actualizar Sección', 'licoteca' ),
        'view_item'                  => __( 'Ver Sección', 'licoteca' ),
        'separate_items_with_commas' => __( 'Separar Secciones por coma', 'licoteca' ),
        'add_or_remove_items'        => __( 'Agregar o remover Secciones', 'licoteca' ),
        'choose_from_most_used'      => __( 'Escoger de los más Usados', 'licoteca' ),
        'popular_items'              => __( 'Secciones Populares', 'licoteca' ),
        'search_items'               => __( 'Buscar Secciones', 'licoteca' ),
        'not_found'                  => __( 'No hay resultados', 'licoteca' ),
        'no_terms'                   => __( 'No hay Secciones', 'licoteca' ),
        'items_list'                 => __( 'Listado de Secciones', 'licoteca' ),
        'items_list_navigation'      => __( 'Navegación del Listado de Secciones', 'licoteca' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'custom_sections', array( 'elbardetoto' ), $args );

}
add_action( 'init', 'custom_sections', 0 );



/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar_size', 200, 200, true);
    add_image_size('cocteles_img', 378, 378, array('center', 'center'));
    add_image_size('product_img', 300, 300, true);
    add_image_size('blog_img', 600, 280, array('center', 'center'));
    add_image_size('tab_side_big', 280, 280, array('center', 'center'));
    add_image_size('blog_img_small', 600, 200, array('center', 'center'));
    add_image_size('tab_side_small', 280, 200, array('center', 'center'));
}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

require_once('includes/wp_woocommerce_functions.php');

add_action('wp_head', 'custom_url_hook', 10);

function custom_url_hook() {
    echo '<script type="text/javascript"> var ruta_blog = "' . esc_url(get_template_directory_uri()) .'"</script>';
}

/* SERVICE FUNCTION CALLER */
add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );

function ajax_test_enqueue_scripts() {

    wp_enqueue_script( 'popfunctions', get_template_directory_uri() . '/js/pop-functions.js', array('jquery'), '1.0', true );

    wp_localize_script( 'popfunctions', 'popfunctions', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));

}

add_action( 'wp_ajax_nopriv_post_popfunctions', 'post_popfunctions' );
add_action( 'wp_ajax_post_popfunctions', 'post_popfunctions' );

function post_popfunctions() {
    global $woocommerce;

    $datos = $_POST['post_id'];

    $post_product = get_post($datos);
    
?>
<div class="product-pop-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="container">
        <div class="row">
            <div class="product-pop-content col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <button onclick="close_modal()" class="product-pop-close">x</button>
                <div class="product-pop-image col-lg-5 col-md-5 col-sm-5 hidden-xs no-paddingl">
                    <?php $images = rwmb_meta( 'rw_product_silueta', 'size=full_url', $post_product->ID ); ?>
                    <?php  if ( !empty( $images ) ) { foreach ( $images as $image ) { echo "<img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />"; } } ?>
                </div>
                <div class="product-pop-content-info col-lg-7 col-md-7 col-sm-7 col-xs-12 no-paddingr">
                    <h2><?php echo $post_product->post_title; ?></h2>
                    <h3>Origen / <strong>País: </strong> <?php echo get_post_meta($post_product->ID, 'rw_product_origen', true); ?> </h3>
                    <h3>Grado de Alcohol:  <?php echo get_post_meta($post_product->ID, 'rw_product_grade', true); ?></h3>
                    <h3>Temperatura de Servido: <?php echo get_post_meta($post_product->ID, 'rw_product_temp', true); ?></h3>
                    <div class="product-pop-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php echo wpautop($post_product->post_content); ?>
                    </div>
                    <div class="product-pop-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php $product = new WC_Product( $post_product->ID ); ?>
                        <?php echo $product->get_price_html(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
    die();
}

add_filter( 'jetpack_enable_open_graph', '__return_false' );

?>
