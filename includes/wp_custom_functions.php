<?php
/** FUNCION PARA COLOCAR TIEMPO EN ENTRADAS **/
function licoteca_time_ago() {
    global $post;
    $date = get_post_time('G', true, $post);
    $chunks = array(
        array( 60 * 60 * 24 * 365 , __( 'año', 'licoteca' ), __( 'años', 'licoteca' ) ),
        array( 60 * 60 * 24 * 30 , __( 'mes', 'licoteca' ), __( 'meses', 'licoteca' ) ),
        array( 60 * 60 * 24 * 7, __( 'semana', 'licoteca' ), __( 'semanas', 'licoteca' ) ),
        array( 60 * 60 * 24 , __( 'dia', 'licoteca' ), __( 'dias', 'licoteca' ) ),
        array( 60 * 60 , __( 'hora', 'licoteca' ), __( 'horas', 'licoteca' ) ),
        array( 60 , __( 'minuto', 'licoteca' ), __( 'minutos', 'licoteca' ) ),
        array( 1, __( 'segundo', 'licoteca' ), __( 'segundos', 'licoteca' ) )
    );
    if ( !is_numeric( $date ) ) {
        $time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
        $date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
        $date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
    }
    $current_time = current_time( 'mysql', $gmt = 0 );
    $newer_date = time( );
    $since = $newer_date - $date;
    if ( 0 > $since )
        return __(  ' un momento', 'licoteca' );
    for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        if ( ( $count = floor($since / $seconds) ) != 0 )
            break;
    }
    $output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
    if ( !(int)trim($output) ){
        $output = '0 ' . __( 'segundos', 'licoteca' );
    }
    return $output;
}

/** QUITAR ACENTOS Y CARACTERES ESPECIALES - USADO PARA SLUGS DE CATEGORIAS **/
function normalize ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

/* CUSTOM EXCERPT */
function get_excerpt($count){
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus" href="'.$permalink.'">+</a>';
    return $excerpt;
}

/** META TWITTER PARA USUARIO EN EL WORDPRESS **/
function custom_contact_fields( $contact_meta ) {
    if ( !isset( $contact_meta['twitter'] ) ) {
        $contact_meta['twitter'] = 'Usuario de Twitter';
    }
    return $contact_meta;
}
add_filter( 'user_contactmethods', 'custom_contact_fields' );

/** ORDENAR BUSQUEDA POR FECHA **/
function my_search_query( $query ) {
    // not an admin page and is the main query
    if ( !is_admin() && $query->is_main_query() ) {
        if ( is_search() ) { $query->set( 'orderby', 'date' ); }
    }
}
add_action( 'pre_get_posts', 'my_search_query' );

/* BREADCRUMBS */
function the_breadcrumb() {
    echo '<ol class="breadcrumb">';
    if (!is_home()) {
        echo '<li><a href="' . home_url('/') . '">'. __( 'Inicio', 'licoteca' ) . '</a></li>';
        if (is_category() || is_single()) {
            echo '<li>' . get_the_category('title_li=') . '</li>';
            if (is_single()) {
                echo '<li class="active">' . get_the_title() . '</li>';
            }
        } elseif (is_page()) {
            echo '<li class="active">' . get_the_title() . '</li>';
        }
    }
    echo '</ol>';
}

/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );


function sum_video_parser($url, $autoplay, $wdth=320, $hth=320){
    $iframe = '';
    $thumb_link = '';
    if (isset($autoplay)){
        if ($autoplay == true){
            $auto = '?autoplay=0';
        } else {
            $auto = '?autoplay=0';
        }
    }
    if (strpos($url, 'youtube.com') !== FALSE) {
        $step1=explode('v=', $url);
        $step2 =explode('&amp;',$step1[1]);
        if (strlen($step2[0]) > 12){
            $step3 = substr($step2[0], 0, 11);
        } else {
            $step3 = $step2[0];
        }
        $iframe ='<iframe width="'.$wdth.'" height="'.$hth.'" src="http://www.youtube.com/embed/'.$step2[0].$auto.'&showinfo=0&modestbranding=1&rel=0" frameborder="0"></iframe>';
        $embed ='<object width="'.$wdth.'" height="'.$hth.'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="src" value="http://www.youtube.com/v/'.$step2[0].'&'.$auto.'" /><param name="wmode" value="transparent" /><param name="embed" value="" /><embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'.$step2[0].$auto.'" wmode="transparent" embed="" /></object>';
        $thumbnail_str = 'http://img.youtube.com/vi/'.$step3.'/2.jpg';
        $fullsize_str = 'http://img.youtube.com/vi/'.$step3.'/0.jpg';
        $thumb_link = htmlentities($fullsize_str);

    }
    else if (strpos($url, 'vimeo') !== FALSE) {

        $id=str_replace('http://vimeo.com/','',$url);
        $embedurl = "http://player.vimeo.com/video/".$id;

        $iframe = '<iframe  src="'.$embedurl.'"
 width="'.$wdth.'" height="'.$hth.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen>
</iframe>';
        $embed ='
<embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash"
src="'.$embedurl.'"  />
';
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        if (!empty($hash) && is_array($hash)) {
            //$video_str = 'http://vimeo.com/moogaloop.swf?clip_id=%s';
            $thumbnail_str = $hash[0]['thumbnail_small'];
            $fullsize_str = '<img src="'.$hash[0]['thumbnail_large'].'" />';

        }

    }


    return array("embed"=>$iframe,"thumb_image" =>$thumb_link);

}

function licoteca_comment($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
?>
<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <?php endif; ?>
        <div class="comment-author vcard">
            <div class="col-md-1">
                <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
            </div>
            <div class="col-md-11">
                <?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); echo '<br/>'; printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?>
            </div>
        </div>
        <?php comment_text(); ?>
        <?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
        <br />
        <?php endif; ?>
        <div class="reply">
            <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </div>

        <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php
}

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'my_theme_archive_title' );
    ?>
